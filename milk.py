#!/usr/bin/env python3 
import turtle
import random

def hexcon(num):
	key = "0123456789abcdef"
	h = ''
	h16 = int(num/16)
	h1 = num % 16
	h = key[h16]+ key[h1]
	return h

w = turtle.Screen()
w.clear()
w.bgcolor("#000000")
t = turtle.Turtle()
t.width(5)
for n in range(0,361,1):
	red = random.randint(0,255)
	green = random.randint(0,255)
	blue = random.randint(0,255)
	print(red,green,blue)
	rbin = bin(red)
	gbin = bin(green)
	bbin = bin(blue)
	print("bin ",rbin,gbin,bbin)
	rhex = hexcon(red); ghex = hexcon(green); bhex = hexcon(blue)
	print("hex ",rhex,ghex,bhex)
	hexcolor = "#"+rhex+ghex+bhex
	t.pencolor(hexcolor)
	if ( n % 2 == 0 ) :
		t.speed(900)
		t.goto(0,0)
		t.forward(1000)
		t.seth(n)


w.exitonclick()
